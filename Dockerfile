FROM openjdk:8
LABEL Khaleel ALakhras (Khaleel.alakhras@progressoft.com)
EXPOSE 8090
COPY target/assignment-*.jar /usr/local/app.jar
ENTRYPOINT java -jar -Dspring.profiles.active=mysql -Dserver.port=8090 -Dspring.datasource.url=jdbc:mysql://mysql/assignment?allowPublicKeyRetrieval=true -Dspring.datasource.username=root -Dspring.datasource.password=root /usr/local/app.jar