#!/bin/sh

export user=${DOCKER_HUB_REGISTRY_USER}
export pass=${DOCKER_HUB_REGISTRY_PASS}
export GIT_COMMIT_DATE=$(date +%y%m%d --date="@$(git show -s --format=%ct)")
docker build -t $DOCKER_HUB_REGISTRY_USER/akhras:$GIT_COMMIT_DATE-$CI_COMMIT_SHORT_SHA -t $DOCKER_HUB_REGISTRY_USER/kha:latest .
echo "pass" | docker login --username "user" --password-stdin
docker push $user/akhras:$GIT_COMMIT_DATE-$CI_COMMIT_SHORT_SHA